module.exports = function(grunt) {

 grunt.initConfig({
   sass: {
     development: {
       files: {
         'css/apistyle.css': 'scss/apistyle.scss'
       },
     }
   },

   watch: {
     styles: {
       files: ['scss/*.scss'],
       tasks: ['buildsass'],
       options: {
         nospawn: true,
         livereload: true,
       }
     },
   },
 
});


 grunt.loadNpmTasks('grunt-contrib-sass');
 grunt.loadNpmTasks('grunt-contrib-watch');
 grunt.loadNpmTasks('grunt-autoprefixer');

 grunt.registerTask('buildsass', ['sass']);
 grunt.registerTask('default', ['buildsass', 'watch']);
};