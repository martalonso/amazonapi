<?php 
		define('DB_HOST', 'localhost');
		define('DB_USER', 'clevert');
		define('DB_PASSWORD', 'Coca-Cola_32!');
		define('DB_NAME', 'amazonAPI');

		class DatabaseController {
			
			private $db;

			public function __construct() {
				$this->db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
				if ($this->db->connect_errno > 0)
					die('Database problem :(');
				$this->db->set_charset("utf8");
			}

			public function __destruct() {
				$this->db->close();
			}

			public function query($sql) {
				if (!$result = $this->db->query($sql))
					return false;
				return $result;
			}
		}