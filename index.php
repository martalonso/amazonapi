<?php include 'functions.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Amazon API</title>
    <link rel="stylesheet" type="text/css" href="css/apistyle.css">
    <meta name="robots" content="noindex,nofollow"/>
</head>
<body>
      <nav class="menu">
        <div class="searchgroup">         
            <label>Buscar </label>
           <div class="formblock">
              <form id="searchform" class="searchform" action="amazonAPI.php" method="POST">
                <div>
                 <span class="drop" >Todos los departamentos</span>
                  <i class="arrow"></i> 
                  <select class="dropdownmenu" name="dropdownmenu">
                    <?php generateDropdown(); ?>
                  </select>  
                </div>                                              
                <input type="text" placeholder="Búsqueda" class="search" name="search" />           
                <div class="search-icon-box">
                  <label for="searchbox"></label>
                  <input type="submit" class="searchbox" id="searchbox" name="searchbox" value=""/>
                </div>  
              </form> 
           </div>                     
        </div>  
    </nav>

  <div class="content">
    <?php if(!isset($_POST['searchbox'])){
            listItems();
          }else{
            search();
          }
    ?>      
  </div>
  <div class="clear"></div>

</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="js/script.js"></script>
</html>


