<?php

	include 'db.php';
	$db = new DatabaseController();

	// Datos
	function listItems(){
		global $db;
		$sql= $db->query("SELECT title, price, url, image, category, review, rate from items");

		echo '<ul id="listitems">';
			items($sql);
		echo '</ul>';
	}

	// Menu dropdown
	function generateDropdown(){
		global $db;
		$sql = $db->query("SELECT DISTINCT category from items");
		$cont = mysqli_num_rows($sql);

		echo '<option value="Todos los departamentos">Todos los departamentos</option>';
			while($row= mysqli_fetch_array($sql)){
				$cat = $row['category'];	
					
				 if ($_POST['dropdownmenu'] == $cat) { 
				 	echo "<option value='$cat' selected= 'selected'><a href='#''>".$cat."</a></option>"; 
				 }else {	
				 	echo "<option value='$cat'><a href='#''>".$cat."</a></option>"; 
				 } 
			}	
		}

	// dots
	function truncate($string,$length=100,$append="&hellip;") {
  	$string = trim($string);
		  if(strlen($string) > $length) {
		    $string = wordwrap($string, $length);
		    $string = explode("\n", $string, 2);
		    $string = $string[0] . $append;
		  }

		  return $string;
	}

	// stars
  function stars($rate){
		 if($rate == 1) { echo 'star star-1'; }
      if($rate == 2)	{	echo 'star star-2'; }
      if($rate == 3)	{	echo 'star star-3'; }
      if($rate == 4)	{	echo 'star star-4'; }
      if($rate == 5)	{	echo 'star star-5'; }
      if($rate >1 && $rate < 2) { echo 'star star-15'; }
      if($rate >2 && $rate < 3) { echo 'star star-25'; }
      if($rate >3 && $rate < 4) { echo 'star star-35'; }
      if($rate >4 && $rate < 5) { echo 'star star-45'; }
  }

  // busqueda 
	function search(){
		global $db;

		if (isset($_POST['search']) && isset($_POST['dropdownmenu']) ){ 				
					$search = $_POST['search'];
					$dropdownmenu = $_POST['dropdownmenu'];		
	        $all = 'Todos los departamentos';
					
	 			if($dropdownmenu == $all && $search == ''){	
						$sql = $db->query("SELECT * from items");		
						$num = mysqli_num_rows($sql);
						echo "<p class='result'>".$num." resultados en <span>".$dropdownmenu." </span></p>";
				} else if($dropdownmenu !== $all && $search == ''){ 
						$sql = $db->query("SELECT * from items WHERE title like '%$search%' and category ='$dropdownmenu'");
						$num = mysqli_num_rows($sql);
						echo "<p class='result'>".$num." resultados para <strong>".$search."</strong> en <span>".$dropdownmenu."</span> </p>";
				} else {
						$sql = $db->query("SELECT * from items WHERE title like '%$search%' and category ='$dropdownmenu'");
						$num = mysqli_num_rows($sql);
						echo "<p class='result'>".$num." resultados para <strong>".$search."</strong> en <span>".$dropdownmenu."</span> </p>";
				}

		}
				echo '<ul id="listitems">';
					items($sql);
				echo '</ul>';
	}

	// lista items
	function items($sql){
	while($row= mysqli_fetch_array($sql)){
	$tit =  $row['title'];
	$title = explode('-',$tit);
	$rate = $row['rate'];

	?>	
	<li>
		<div class="item">
			<div class="image-item">
					<a href="<?php echo $row['url'] ?>"><img src="<?php echo $row['image'] ?>"></a>
			</div>
			
			<div class="item-desc">
					<a href="<?php echo $row['url'] ?>"><h2><?php echo truncate($row['title']) ?></h2></a>
					<div class="items">
						<span class="price"><?php if($row['price']!=null) echo 'EUR'.$row['price']. '€' ?></span>
						<span class="category"><?php echo $row['category'] ?></span>
						<span class="rate">
							<a href="<?php echo $row['review'] ?>"><i class='<?php stars($rate); ?>'></i>
								
							</a>
							<p class="rate-star"><?php echo $rate ?></p>
						</span>
					</div>
			</div>
		</div>
	</li>			
	<?php
	}
	}


